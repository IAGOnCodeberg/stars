#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char randChar (int density) {
    if (rand() % density == 0) {
        const char *startypes = "******...OO@";
        return startypes[rand() % strlen(startypes)];
    }
    else {
        return ' ';
    }
}

int main (int argc, char **argv) {
    int density = 30;
    if (argc > 1) {
        if (atoi(argv[1]) <= 0) {
            fprintf(stderr, "Warning: Density must be an integer > 0, ");
            fprintf(stderr, "setting density to %d\n", density);
        }
        else {
            density = atoi(argv[1]);
        }
    }

    WINDOW *win = initscr();
    long sizex;
    long sizey;
    getmaxyx(win, sizey, sizex);
    for (long i = 0; i < sizey * sizex; ++i) {
        addch(randChar(density));
    } 
    refresh();
    while (1) {
        char input = wgetch(win);
        if (input == 'q' || input == 'Q') {
            break;
        }
    }
    endwin();
    return 0;
}
