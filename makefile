all: run

run: build
	@./stars

build:
	@gcc -Wall -lncurses -o stars stars.c

clean:
	@rm -f stars